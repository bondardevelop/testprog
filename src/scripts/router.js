import View from './user/view.js';
import Model from './user/model.js';
let products = window.dataList = Model.CallApi();

export default function(data) {

	const router = {
		set(routeType, products){
			let body = document.querySelector('body');
			View.cleanPage();
			View.productsRender(routeType, products);
			body.removeAttribute("class");
			body.classList.add(routeType);
		}
	};

	router.set('admin', products);

	document.querySelector('.navbar').addEventListener('click', ({target}) => {
		let buttons = document.querySelectorAll(".button-group");
		Array.prototype.forEach.call(buttons, function (button) {
			button.classList.remove("active");
		});
		if(target.classList.contains('button-group') && !target.classList.contains('active')){
			router.set(target.dataset.route, products);
			window.history.pushState( {}, target.dataset.route, (`/#${target.dataset.route}`) );
			target.classList.add("active");
		}
	});

}

