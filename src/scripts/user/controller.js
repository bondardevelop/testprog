import Model from './model.js';
let order = false;
let products = {};
products.data = Model.CallApi();

export default {
	deleteProduct(productid, products) {
		let newList = products.filter((item) => { 
			return item.id != productid;
		});
		Model.DeleteItem(productid);
		return newList;
	},

	filterId() {
		let sorted = products.data.sort((a, b) => {
			if (order) {
				return parseFloat(b.id) - parseFloat(a.id);
			}
			else {
				return parseFloat(a.id) - parseFloat(b.id);
			}			
		});
		order = !order;
		products.data = sorted;
		return sorted;
	},

	filterPrice() {
		let sorted = products.data.sort((a, b) => {
			let aPrice = parseFloat(a.price.replace(/\.0+/, '.').replace('.', ""));
			let bPrice = parseFloat(b.price.replace(/\.0+/, '.').replace('.', ""));
			if (order) {
				return bPrice - aPrice;
			}
			else {
				return aPrice - bPrice;
			}			
		});
		order = !order;
		products.data = sorted;
		return sorted;
	},

	filterAvalible(products) {
		let newList = products.filter((item) => { 
			if (order) {
				return item.available == true;
			}
			else {
				return item.available == false;
			}			
		});
		order = !order;
		return newList;
	},
}



