import Model from './model.js';


export default {
	cleanPage() {
		document.querySelector('#app').innerHTML = '';
	},
	productsRender(pageType, products) {

		let cols ='col-md-4';

		if (pageType=='admin') {
			cols = 'col-md-12';

			let productWrapperAddProduct = document.createElement('div');
			productWrapperAddProduct.className = "add_product_wrapper";
			productWrapperAddProduct.innerHTML = "<div class='add_product add_product_btn btn btn-success'>Add product</div>";

			let productWrapperColNames = document.createElement('div');
			productWrapperColNames.className = `${cols} productWrapper productWrapper-col-names`;
			productWrapperColNames.innerHTML = `
			<div class="card_id">id</div>
			<div class="card_desc">desc</div>
			<div class="card_img">img</div>
			<div class="card_avalible">available</div>
			<div class="card_price">price</div>
			<div class="card_event">event</div>
			`;

			let productWrapperFilters = document.createElement('div');
			productWrapperFilters.className = `${cols} productWrapper productWrapper-filters`;
			productWrapperFilters.innerHTML = `
			<div class="btn btn-info filter_id" id="filter_id">filter id</div>
			<div class="filter_available_wrapp">
				<div class="btn btn-info filter_available" id="filter_available">filter available</div>
				<div class="btn btn-info show_all_available" id="show_all_available">show all</div>
			</div>			
			<div class="btn btn-info filter_price" id="filter_price">filter price</div>
			`;
			
			document.querySelector('#app').append(productWrapperAddProduct);
			document.querySelector('#app').append(productWrapperFilters);
			document.querySelector('#app').append(productWrapperColNames);
		}

		for (var i = 0; i < products.length; i++) {
			let cardId        = products[i]['id'];
			let cardDesc      = products[i]['desc'];
			let cardImg       = products[i]['img'];
			let cardAvailable = products[i]['available'];
			let cardPrice     = products[i]['price'];

			if (cardAvailable) {
				cardAvailable = 'Available';
			}
			else {
				cardAvailable = 'NOt available';
			}

			let productWrapper = document.createElement('div');
			productWrapper.className = `${cols} productWrapper productWrapper-${cardId}`;
			productWrapper.id =  `edit-${cardId}`;
			if (pageType=='user') {
				productWrapper.innerHTML = `
				<div class="card_desc">${cardDesc}</div>
				<img class="card_img" src=${cardImg}>
				<div class="card_avalible">${cardAvailable}</div>
				<div class="card_price">${cardPrice}</div>
				`;
			}
			else {
				productWrapper.innerHTML = `
				<div class="card_id">${cardId}</div>
				<div class="card_desc">${cardDesc}</div>
				<img class="card_img" src=${cardImg}>
				<div class="card_avalible">${cardAvailable}</div>
				<div class="card_price">${cardPrice}</div>
				<div class="edit_delete"><div class="edit btn btn-info" data-itemid=${cardId}>edit</div><div class="delete btn btn-danger" data-itemid=${cardId}>delete</div></div>
				`;
			}
			
			document.querySelector('#app').append(productWrapper);

		}
  },
  productUpdate(itemId) {
		let itemWrapper = document.querySelector(`#edit-${itemId}`);
		let descriptionBlock = itemWrapper.querySelector('.card_desc');
		let avalibleBlock = itemWrapper.querySelector('.card_avalible');
		let priceBlock = itemWrapper.querySelector('.card_price');
		let saveButton = itemWrapper.querySelector('.edit');

		let input = document.createElement("INPUT");
		input.value = descriptionBlock.innerHTML;
		input.setAttribute("type", "text");
		descriptionBlock.append(input);		

		let arraySelectList = [true,false];
		let selectList = document.createElement("select");
		selectList.className = "edit_prod_select_input";
		avalibleBlock.appendChild(selectList);
		for (var i = 0; i < arraySelectList.length; i++) {
			var option = document.createElement("option");
			option.value = arraySelectList[i];
			option.text = arraySelectList[i];
			selectList.appendChild(option);
		}

		let input3 = document.createElement("INPUT");
		input3.value = priceBlock.innerHTML;
		input.setAttribute("type", "text");
		priceBlock.append(input3);

		saveButton.classList.remove("edit");
		saveButton.classList.add("save");
		saveButton.innerHTML = "save";
	},
	productAfterSave(itemId) {
		let itemWrapper = document.querySelector(`#edit-${itemId}`);
		let inputs = itemWrapper.querySelectorAll('input');	
		let select = itemWrapper.querySelector('select');	
		let saveButton = itemWrapper.querySelector('.save');

		Array.prototype.forEach.call(inputs, function (input) {
			input.remove();
		});	
		select.remove();
		
		saveButton.classList.remove("save");		
		saveButton.innerHTML = "edit";
		setTimeout(() => { saveButton.classList.add("edit") }, 100);
	},
	addProductInputs(newProdId) {
		let addProductWrapper = document.querySelector(".add_product_wrapper");
		let addProductButton = document.querySelector(".add_product");

		let newProdIdBlcok = document.createElement("div");
		newProdIdBlcok.className = "new_prod_id";
		newProdIdBlcok.innerHTML = newProdId;
		addProductWrapper.append(newProdIdBlcok);

		let input = document.createElement("INPUT");
		input.value = "Product description";
		input.setAttribute("type", "text");
		input.className = "prod_desc_input";
		addProductWrapper.append(input);

		let input2 = document.createElement("INPUT");
		input2.value = "https://fish-fish.com.ua/published/publicdata/STORE/attachments/SC/products_pictures/29460.jpg";
		input2.setAttribute("type", "text");
		input2.className = "prod_img_input";
		addProductWrapper.append(input2);

		let arraySelectList = [true,false];
		let selectList = document.createElement("select");
		selectList.className = "prod_select_input";
		addProductWrapper.appendChild(selectList);
		for (var i = 0; i < arraySelectList.length; i++) {
			var option = document.createElement("option");
			option.value = arraySelectList[i];
			option.text = arraySelectList[i];
			selectList.appendChild(option);
		}

		let input3 = document.createElement("INPUT");
		input3.value = "123.00";
		input3.setAttribute("type", "text");
		input3.className = "prod_price_input";
		addProductWrapper.append(input3);

		addProductButton.classList.remove("add_product");		
		addProductButton.innerHTML = "Save product";
		setTimeout(() => { addProductButton.classList.add("add_product_save"); }, 100);
	},
	addProductSave() {
		let saveProductButton = document.querySelector(".add_product_save");
		let inputs = document.querySelectorAll('.add_product_wrapper input');
		let select = document.querySelector('.add_product_wrapper .prod_select_input');
		let idBlock = document.querySelector('.add_product_wrapper .new_prod_id');

		Array.prototype.forEach.call(inputs, function (input) {
			input.remove();
		});
		idBlock.remove();
		select.remove();		

		saveProductButton.classList.remove("add_product_save");		
		saveProductButton.innerHTML = "Add Product";
		setTimeout(() => { saveProductButton.classList.add("add_product") }, 100);
	},
}
