export default {
	CallApi() {
		let url = 'http://5dd19add15bbc2001448d2f7.mockapi.io/products';
		let xmlHttp = new XMLHttpRequest();
		xmlHttp.open("GET", url, false);
		xmlHttp.send(null);
		return JSON.parse(xmlHttp.responseText);
	},
	DeleteItem(itemId) {
		let url = "http://5dd19add15bbc2001448d2f7.mockapi.io/products/";
		let xhr = new XMLHttpRequest();
		xhr.open("DELETE", url+itemId, true);
		xhr.onload = function () {
			let products = JSON.parse(xhr.responseText);
			if (xhr.readyState == 4 && xhr.status == "200") {
				console.log('Item deleted');
			} else {
				console.error('Item deleted error');;
			}
		}
		xhr.send(null);
	},
	UpdateItem(itemId, data) {
		let url = "http://5dd19add15bbc2001448d2f7.mockapi.io/products/";
		let json = JSON.stringify(data);
		let xhr = new XMLHttpRequest();
		xhr.open("PUT", url+itemId, true);
		xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
		xhr.onload = function () {
			let users = JSON.parse(xhr.responseText);
			if (xhr.readyState == 4 && xhr.status == "200") {
				console.log('Prod item apdated');
			} else {
				console.error('Prod item apdated error');
			}
		}
		xhr.send(json);
	},
	AddItem(data) {
		let url = "http://5dd19add15bbc2001448d2f7.mockapi.io/products/";
		let json = JSON.stringify(data);
		let xhr = new XMLHttpRequest();
		xhr.open("POST", url, true);
		xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
		xhr.onload = function () {
			var users = JSON.parse(xhr.responseText);
			if (xhr.readyState == 4 && xhr.status == "201") {
				console.log('new prod added');
			} else {
				console.error('new prod added error');
			}
		}
		xhr.send(json);
	}
}
