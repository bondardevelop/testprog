import '../styles/index.scss';
import Route from './router.js';
import Model from './user/model.js';
import View from './user/view.js';
import Controller from './user/controller.js';

Route();


document.querySelector('body').addEventListener('click', ({target}) => {
	let products = Model.CallApi();
	if(target.classList.contains('delete')){
		let itemId = parseInt(target.dataset.itemid);
		let newProdList = Controller.deleteProduct(itemId, products);
		View.cleanPage();
		View.productsRender('admin', newProdList);
	}
});

document.querySelector('body').addEventListener('click', ({target}) => {
	if(target.classList.contains('save')){
		let itemId = parseInt(target.dataset.itemid);
		let itemWrapper = document.querySelector(`#edit-${itemId}`);
		let descriptionInput = itemWrapper.querySelector('.card_desc input');
		let avalibleInput = itemWrapper.querySelector('.card_avalible select');
		let priceInput= itemWrapper.querySelector('.card_price input');

		let dataToChange = {
			available: avalibleInput.value,
			desc: descriptionInput.value,
			price: priceInput.value
		}
		Model.UpdateItem(itemId, dataToChange);
		View.productAfterSave(itemId);
	}
});

document.querySelector('body').addEventListener('click', ({target}) => {
	if(target.classList.contains('edit')){
		let itemId = parseInt(target.dataset.itemid);
		View.productUpdate(itemId);
	}
});

document.querySelector('body').addEventListener('click', ({target}) => {
	if(target.classList.contains('filter_id')){
		let newProdList = Controller.filterId();
		View.cleanPage();
		View.productsRender('admin', newProdList);
	}
});

document.querySelector('body').addEventListener('click', ({target}) => {
	if(target.classList.contains('filter_price')){
		let newProdList = Controller.filterPrice();
		View.cleanPage();
		View.productsRender('admin', newProdList);
	}
});

document.querySelector('body').addEventListener('click', ({target}) => {
	if(target.classList.contains('filter_available')){
		let products = Model.CallApi();
		let newProdList = Controller.filterAvalible(products);
		View.cleanPage();
		View.productsRender('admin', newProdList);
	}
});

document.querySelector('body').addEventListener('click', ({target}) => {
	if(target.classList.contains('show_all_available')){
		let products = Model.CallApi();
		View.cleanPage();
		View.productsRender('admin', products);
	}
});

document.querySelector('body').addEventListener('click', ({target}) => {
	if(target.classList.contains('add_product')){
		let products = Model.CallApi();
		let newProdId = 1;
		for (var i = 0; i < products.length; i++) {
			if (parseFloat(products[i].id) == newProdId) {
				newProdId++
			}
		}
		console.log(products,newProdId);
		View.addProductInputs(newProdId);
	}
});

document.querySelector('body').addEventListener('click', ({target}) => {
	if(target.classList.contains('add_product_save')){
		let addProductInputsWrapper = document.querySelector(".add_product_wrapper");
		let newProdId = addProductInputsWrapper.querySelector('.new_prod_id').innerHTML;
		let newProdDesc = addProductInputsWrapper.querySelector('.prod_desc_input').value;
		let newProdImg = addProductInputsWrapper.querySelector('.prod_img_input').value;
		let newProdAvalible = addProductInputsWrapper.querySelector('.prod_select_input').value;
		let newProdPrice = addProductInputsWrapper.querySelector('.prod_price_input').value;

		let newProductData = {
			available: newProdAvalible,
			desc: newProdDesc,
			id: newProdId,
			img: newProdImg,
			price: newProdPrice
		}
		
		Model.AddItem(newProductData);
		View.addProductSave();	
		setTimeout(() => { 
			View.cleanPage();
			View.productsRender('admin', Model.CallApi());
		}, 1000);
		
	}
});
